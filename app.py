from flask import Flask, jsonify, redirect
from flask_restful import Api
from flasgger import Swagger
from resources.task import Task, TaskList, TaskSearch
from flask_cors import CORS

from db import db

import os
import logging

app = Flask(__name__)

api = Api(app, errors={
    'NoAuthorizationError': {
        'message': "La solicitud de contiene el token",
        'error': 'autorization_required',
        'status': 401
    }
})

PREFIX = os.environ.get('PREFIX_PATH', '/api')

app.config['SWAGGER'] = {
   'title': 'upa-backend',
   'version': '1.0.0',
   'description': 'API de servicios REST en Flask',
   'uiversion': 2,
   'tags': [{'name': 'jwt'}],
   'specs': [{
       'endpoint': 'apispec_1',
       'route': f'{PREFIX}/apispec_1.json',
       'rule_filter': lambda rule: True,  # all in
       'model_filter': lambda tag: True  # all in
   }],
   'specs_route': f"{PREFIX}/apidocs/",
   'static_url_path': f'{PREFIX}/static'
}

swagger = Swagger(app)

app.logger.setLevel(logging.INFO)

# Configuracion de parametros de la base de datos
def env_config(name, default):
    app.config[name] = os.environ.get(name, default = default)

env_config('SQLALCHEMY_DATABASE_URI', 'postgresql://postgres:postgres@localhost:5432/todo')
app.config['SQLAlCHEMY_TRACK_MODIFICATIONS'] = False
app.config['PROPAGATE_EXCEPTIONS'] = True
app.config['SQLALCHEMY_ECHO'] = False

@app.route("/")
@app.route(f'{PREFIX}')
def welcome():
    return redirect(f"{PREFIX}/apidocs", code=302)

api.add_resource(Task, f'{PREFIX}/task/<id>')
api.add_resource(TaskList, f'{PREFIX}/task')
api.add_resource(TaskSearch, f'{PREFIX}/search/task')

# enable CORS
CORS(app,resources={r'/*':{'origins': '*'}})
@app.route(f'{PREFIX}/ping', methods=['GET'])
def hello():
    return jsonify('Clase UPA - Dia de la Primavera!')



# Bloque opcional para ejecutar con python app.py
if __name__ == '__main__':
    db.init_app(app) 
    app.run()
else:
    db.init_app(app)