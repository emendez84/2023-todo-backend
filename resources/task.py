from flask_restful import Resource, reqparse
from flasgger import swag_from
from models.task import TaskModel
from utils import paginated_results, restrict
from flask import request

class Task(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('id', type=int)
    parser.add_argument('descrip', type=str)
    parser.add_argument('status', type=str)
    
    # Metodo que obtiene un elemento de tareas
    @swag_from('../swagger/task/get_task.yaml')
    def get(self, id):
        task = TaskModel.find_by_id(id)
        if task:
            return task.json()
        return {'message': 'No se encuentra la tarea solicitada'}, 404
    
    # Metodo para modificar registros creados
    @swag_from('../swagger/task/put_task.yaml')
    def put(self, id):
        task = TaskModel.find_by_id(id)
        if task:
            newdata = Task.parser.parse_args()
            task.from_reqparse(newdata)
            task.save_to_db()
            return task.json()
        
    # Metodo para eliminar un registro
    @swag_from('../swagger/task/delete_task.yaml')    
    def delete(self, id):
        task = TaskModel.find_by_id(id)
        if task:
            task.delete_from_db()
            return {'message': "Se ha borrado la tarea"}
        else:
            return {'message': "Tarea no existente"}
        

class TaskList(Resource):
    @swag_from('../swagger/task/post_task.yaml') 
    def post(self):
        data = Task.parser.parse_args()

        task = TaskModel(**data)

        try:
            task.save_to_db()
        except Exception as e:
            print(e)
            return{'message': 'Ocurrio un error al crear al tarea'}, 500
        
        return task.json(), 201
    
    @swag_from('../swagger/task/list_task.yaml')
    def get(self):
        query = TaskModel.query
        return paginated_results(query)

class TaskSearch(Resource):
    @swag_from('../swagger/task/search_task.yaml') 
    def post(self):
        query = TaskModel.query
        if request.json:
            filtros= request.json
            query = restrict(query, filtros, 'id', lambda x: TaskModel.id == x)
            query = restrict(query, filtros, 'descrip', lambda x: TaskModel.descrip.contains(x))
            query = restrict(query, filtros, 'status', lambda x: TaskModel.status.contains(x))
        return paginated_results(query)
