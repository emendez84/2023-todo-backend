from flask_restful.reqparse import Namespace

from db import db
from models.type import TypeModel 
from utils import _assign_if_something

class TaskModel(db.Model):
   __tablename__ = 'task'

   id = db.Column(db.BigInteger, primary_key=True)
   descrip = db.Column(db.String)
   status = db.Column(db.String)
   type_id = db.Column(db.BigInteger, db.ForeignKey(TypeModel.id))

   _type = db.relationship('TypeModel', foreign_keys=[type_id], uselist=False)

   def __init__(self, id, descrip, status, type_id):
       self.id = id
       self.descrip = descrip
       self.status = status
       self.type_id = type_id


   def json(self, jsondepth=1):
       json =  {
           'id': self.id,
           'descrip': self.descrip,
           'status': self.status,
           'type_id': self.type_id,
       }
       if jsondepth > 0:
           if self._type:
               json['_type'] = self._type.json(jsondepth - 1)
       return json
   
   @classmethod
   def find_by_id(cls, id):
       return cls.query.filter_by(id=id).first()
   
   def save_to_db(self):
       db.session.add(self)
       db.session.commit()
    
   def delete_from_db(self):
       db.session.delete(self)
       db.session.commit()

   def from_reqparse(self, newdata: Namespace):
       for no_pk_key in ['descrip','status','type_id']:
           _assign_if_something(self, newdata, no_pk_key)